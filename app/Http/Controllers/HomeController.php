<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\ExpenseName;
use App\Expense;
use App\Customer;
use App\Medicine;
use App\Manufacturer;
use App\Supplier;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $title = 'Dashboard';
        $icon = 'fa fa-dashboard';

        $sales=DB::table("Sales")->get()->sum("grand_total");
        $sales_due=DB::table("Sales_dues")->get()->sum("due_amount");
        $purchase=DB::table("Purchases")->get()->sum("grand_total");
        $expense=DB::table("Expenses")->get()->sum("amount");
        $parchase_due=DB::table("Purchase_dues")->get()->sum("due_amount");

        $customer=DB::table("Customers")->count();
        $medicine=DB::table("Medicines")->count();
        $manufacturer=DB::table("Medicines")->count();
        $supplier=DB::table("Suppliers")->count();


       $medicine_all = Medicine::orderBy('id', 'asc')->get();
         $sales_medicine = DB::table('medicines')
        ->join('salesinfos', 'medicines.id', '=', 'salesinfos.medicine_id')
        ->groupBy('medicines.id')
        ->selectRaw('*, sum(quantity) as sum')
        ->get();
         $purchase_medicine = DB::table('medicines')
        ->join('purchaseinfos', 'medicines.id', '=', 'purchaseinfos.medicine_id')
        ->groupBy('medicines.id')
        ->selectRaw('*, sum(quantity) as sum')
        ->get();

        return view('layouts.index', compact('title','icon','sales','sales_due','purchase','expense','parchase_due','customer','medicine','manufacturer','supplier','medicine_all','sales_medicine','purchase_medicine'))->with('no', 1);
    }


}
